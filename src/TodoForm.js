import { Component } from "react";

import Button from './components/Button'

import {
    FormControl,
    FormLabel,
    Input,
    Textarea,

} from "@chakra-ui/react"
import TodoList from "./TodoList";


class TodoForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            todo: [{
                title: '',
                description: '',
            }]
        }
    }

    handleChange = (e) => {

        const { name, value } = e.target
        // console.log(name, value)
        this.setState({ ...this.state.todo, [name]: value })

    }

    handleSubmit = (e) => {
        e.preventDefault()

        const {
            todo
        } = this.state

        // const { name, value } = e.targetx/



        console.log(todo)

        // this.setState({
        //     todo: [name]: value
        // })
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <FormControl>
                        <FormLabel>Title</FormLabel>
                        <Input type="text" name='title' onChange={this.handleChange} />
                        <br /><br />
                        <FormLabel>Description</FormLabel>
                        <Textarea onChange={this.handleChange} name='description'></Textarea>
                        <br /><br />
                        <Button type='submit' text='click' />
                    </FormControl>
                </form>
                <br /><br />

                <TodoList />
            </div>
        )
    }
}

export default TodoForm