import { Component } from "react";

import styled from "styled-components";

import { Flex } from "@chakra-ui/layout";


const Input = styled.input`
  padding: 0.5em;
  margin: 0.5em;
  color: palevioletred;
  background: papayawhip;
  border: none;
  border-radius: 3px;
`;

class InputField extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <Flex>
                <Input type={this.props.type} name={this.props.name} />

            </Flex>
        )
    }
}

export default InputField