import { Component } from "react";
import { chakra } from "@chakra-ui/react"

class Button extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <chakra.button
                px="3"
                py="2"
                bg="green.200"
                rounded="md"
                _hover={{ bg: "green.300" }}
                type={this.props.type}
            >
                {this.props.text}
            </chakra.button>
        )
    }
}

export default Button