import { Component } from "react";

import {
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    TableCaption,
} from "@chakra-ui/react"


class TodoList extends Component {

    render() {
        return (
            <Table variant="simple">
                <TableCaption>Imperial to metric conversion factors</TableCaption>
                <Thead>
                    <Tr>
                        <Th>Title</Th>
                        <Th>description</Th>
                        <Th>status</Th>
                    </Tr>
                </Thead>
                <Tbody>
                    <Tr>
                        <Td>inches</Td>
                        <Td>millimetres (mm)</Td>
                        <Td >25.4</Td>
                    </Tr>


                </Tbody>

            </Table>
        )
    }
}

export default TodoList