
import './App.css';
import { ChakraProvider, Container } from "@chakra-ui/react"
import * as React from "react"

import TodoForm from './TodoForm';

function App() {
  return (
    <ChakraProvider>
      {/* <InputField /> */}
      <Container>

        <TodoForm />
        
      </Container>
    </ChakraProvider>


  );
}

export default App;
